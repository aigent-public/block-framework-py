FROM docker-registry.int.aigent.co/aigent-public/block-framework:latest

WORKDIR /opt/blockFramework

RUN apt update && apt install -y python3
RUN ln -sf /usr/bin/python3 /usr/bin/python

CMD tail -f /dev/null
